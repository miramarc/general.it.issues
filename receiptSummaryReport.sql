USE [VISTA]
GO
/****** Object:  StoredProcedure [dbo].[spRptReceiptsSummary]    Script Date: 2020/10/16 下午 04:57:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[spRptReceiptsSummary]
	@fromdate [datetime],
	@todate [datetime],
	@GroupByCinOperator [nvarchar](10)
AS
DECLARE	@CinemaCode nvarchar(4),
        @strDefaultCinOperatorCode nvarchar(10)
SELECT @CinemaCode = Cinema_strCode FROM tblControl
SELECT TOP 1 @strDefaultCinOperatorCode = CinOperator_strCode FROM tblCinema_Operator WHERE CinOperator_strDefault = 'Y'
/* Create a temporary table to summarise all data into so that the payment type can be worked out more accurately */
/*
Create TABLE #tblTempRptReceiptSum 	(
					TransC_lgnNumber	 Integer	Null,
					TransC_strType		 nvarchar(1)	Null,
					TransC_strBKCardNo	 nvarchar(30)	Null,
					TransC_strBKCardType	 nvarchar(50)	Null,
					TransC_strBKSource	 nvarchar(50)	Null
/*					)*/
--INSERT INTO #tblTempRptReceiptSum	
--(
	--	TransC_lgnNumber,
	--	TransC_strType,
	--	TransC_strBKCardNo,
	--	TransC_strBKCardType,
	--	TransC_strBKSource)*/
	Select cash.TransC_lgnNumber,
		cash.TransC_intSequence,
		cash.TransC_strType,
		cash.TransC_strBKCardNo,
		(CASE  WHEN cash.TransC_strBKCardType = '' OR cash.TransC_strBKCardType IS NULL OR UPPER(cash.TransC_strBKCardType) = 'OTHER' OR UPPER(cash.TransC_strBKCardType) = 'UNKNOWN' 
							THEN ISNULL((Select top 1 UPPER(Card_strCardType)
							from tblCardDefinition 
							where Left(cash.TransC_strBKCardNo,6) >= Card_strStartRange 
							AND left(cash.TransC_strBKCardNo,6) < Card_strEndRange 
							AND Upper(Card_strPaymentCard) = 'Y' 
							AND (cash.TransC_strBKCardNo <> ''
							OR   cash.TransC_strBKCardNo IS NOT NULL)
							order by Card_intRangeSize Asc) , 'UNKNOWN')
							ELSE UPPER(cash.TransC_strBKCardType) END) as TransC_strBKCardType,
		cash.TransC_strBKSource 
	INTO 	#tblTempRptReceiptSum
From tblTrans_Cash cash WITH (INDEX=indDateTime), tblPaymentType pay
Where cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate 
--AND cash.TransC_curValue >= 0 --Commented that out because it was excluding Refunds MP 2003/11/11
AND cash.TransC_strType = pay.PayType_strType	
-- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
AND 'Y' IN ( pay.PayType_strCreditCard, pay.PayType_strDebitCard ) --Select only credit/debit card transactions
 
/*
DECLARE @CardNumber 	nvarchar(50)	
DECLARE @TransNum 	Integer
DECLARE @CardType	nvarchar(10)
DECLARE @TransType	nvarchar(1)
DECLARE @TransSeq	Integer
DECLARE TableCursor CURSOR 
	FORWARD_ONLY
	FOR SELECT DISTINCT TransC_strBKCardNo, TransC_strBKCardType, TransC_lgnNumber, TransC_intSequence, TransC_strType FROM #tblTempRptReceiptSum
	
/* Use a cursor to scroll through all records in the temp table AND try AND work out which credit card type it is.
	NOTE: POS does not write a credit card number to the transaction table so it cannot be worked out AND is 
	recorded as Credit Card
*/
OPEN TableCursor
FETCH NEXT From TableCursor INTO @CardNumber, @CardType, @TransNum, @TransSeq, @TransType
WHILE @@FETCH_STATUS = 0
BEGIN
	 
	If @CardType = '' OR @CardType Is NULL OR @CardType = 'OTHER' OR @CardType = 'UNPAID'
	
	Begin
	If Len(@CardNumber) = 16
	Begin
		SET @CardType = 
		CASE
			When SubString(@CardNumber, 1, 2) Between 51 AND 55 then 'MASTERCARD'
			When SubString(@CardNumber, 1,1) = 4 then 'VISA'
			When SubString(@CardNumber, 1,1) = 3 then 'JCB'
		End
	End
	
	If(Len(@CardNumber) = 13)
	Begin
		Set @CardType = 
		CASE
			When (SubString(@CardNumber, 1,1) = 4)  then 'VISA'
		End
	End
	
	If Len(@CardNumber) = 15
	Begin
		Set @CardType =
		Case
			When SubString(@CardNumber, 1, 2) = 34 then 'AMEX'
			When SubString(@CardNumber, 1, 2) = 37 then 'AMEX'
			When SubString(@CardNumber, 1, 4) = 2014 then 'ENROUTE'
			When SubString(@CardNumber, 1, 4) = 2149 THEN 'ENROUTE'
			When SubString(@CardNumber, 1,4) = 2031 then 'JCB'
			When SubString(@CardNumber, 1,4) = 1800 then 'JCB'
		End
	End
	--Diners Club/Carte Blanche
	If Len(@CardNumber) = 14
	Begin
		Set @CardType = 
		Case
			When SubString(@CardNumber, 1, 2) = 36 then 'DINERS'
			When SubString(@CardNumber, 1, 2) = 38 then 'DINERS'
			When SubString(@CardNumber, 1, 3) Between 300 AND 305 then 'DINERS'
		End
	End
	If @CardType Is NULL OR @CardType = '' OR @CardType = 'UNPAID'
		Begin
		/* Select the name name from the payment type table for the payment type */
		Select @CardType = UPPER(PayType_strDescription) from tblPaymentType Where PayType_strType = @TransType
		
	End
	
	Update #tblTempRptReceiptSum Set TransC_strBKCardType = @CardType Where TransC_lgnNumber = @TransNum AND TransC_intSequence = @TransSeq
	END
	FETCH NEXT From TableCursor INTO @CardNumber, @CardType, @TransNum, @TransSeq, @TransType
	
End
CLOSE TableCursor
DEALLOCATE TableCursor
	*/
/* Delete The Report Transaction table  */
DELETE tblRptReceiptSum
/*	Summarise Payment types by Workstation Group given a date range.	*/
/*	Provide columns Gross, Refunds AND Net.					*/
/*	Provide GrANDs Totals for each payment type.				*/
/* 	Retrieve Cinema Code							*/
/* Gross				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
)
SELECT 	'G',
		'',
		SUM(Cash.TransC_curValue), 
		tblWorkstation_Group.WGroup_strDescription,
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue >= 0 AND
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
				(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
				tblPaymentType.PayType_strDescription	
				ELSE
				tblPaymentType.PayType_strTenderCategory END),
				tblPaymentType.PayType_strDescription,
				ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		    ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Refunds				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'R' ,
		'',
		ABS(SUM(Cash.TransC_curValue)), 
		tblWorkstation_Group.WGroup_strDescription,
		tblPaymentType.PayType_strDescription,
			(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
	tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
	Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
	tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
	Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
	Cash.TransC_curValue < 0 AND
  -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
  'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
				(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
				tblPaymentType.PayType_strDescription	
				ELSE
				tblPaymentType.PayType_strTenderCategory END),
				tblPaymentType.PayType_strDescription,
				ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		    ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
	
/* Net			*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'N' ,
		'',
		SUM(Cash.TransC_curValue), 
		tblWorkstation_Group.WGroup_strDescription,
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
	tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
	Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
	tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
	Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
  -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
  'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
			(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
			tblPaymentType.PayType_strDescription	
			ELSE
			tblPaymentType.PayType_strTenderCategory END),
			tblPaymentType.PayType_strDescription,
			ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		  ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Gross Totals				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'G' ,
		'',
		SUM(Cash.TransC_curValue), 
		'TOTALS',
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue >= 0 AND
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		tblPaymentType.PayType_strDescription,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Refunds Totals				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'R' ,
		'',
		ABS(SUM(Cash.TransC_curValue)), 
		'TOTALS',
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue < 0 AND
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
			(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
			tblPaymentType.PayType_strDescription	
			ELSE
			tblPaymentType.PayType_strTenderCategory END),
			tblPaymentType.PayType_strDescription,
			ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
	    ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
	
/* Net Totals			*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'N' ,
		'',
		SUM(Cash.TransC_curValue), 
		'TOTALS',
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
			(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		tblPaymentType.PayType_strDescription,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Gross Cinema Operator Totals				*/
IF @GroupByCinOperator = 'Y'
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'G' ,
		'',
		SUM(Cash.TransC_curValue), 
		'CIN OP TOTALS',
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue >= 0 AND
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	--tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		tblPaymentType.PayType_strDescription,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Refunds Cinema Operator Totals				*/
IF @GroupByCinOperator = 'Y'
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'R' ,
		'',
		ABS(SUM(Cash.TransC_curValue)), 
		'CIN OP TOTALS',
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue < 0 AND
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	--tblWorkstation_Group.WGroup_strDescription,
			(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
			tblPaymentType.PayType_strDescription	
			ELSE
			tblPaymentType.PayType_strTenderCategory END),
			tblPaymentType.PayType_strDescription,
			ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
	    ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
	
/* Net Cinema Operator Totals			*/
IF @GroupByCinOperator = 'Y'
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strPayTypeDesc,
		RS_strTenderCategory,  
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'N' ,
		'',
		SUM(Cash.TransC_curValue), 
		'CIN OP TOTALS',
		tblPaymentType.PayType_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    'Y' NOT IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	--tblWorkstation_Group.WGroup_strDescription,
			(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		tblPaymentType.PayType_strDescription	
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		tblPaymentType.PayType_strDescription,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Now get Credit Card information using the temp table to find out the type of transaction */
/* Gross				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
)
SELECT 	'G',
		'',
		SUM(Cash.TransC_curValue), 
		tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue >= 0 
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
			(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
			#tblTempRptReceiptSum.TransC_strBKCardType
			ELSE
			tblPaymentType.PayType_strTenderCategory END),
			#tblTempRptReceiptSum.TransC_strBKCardType,
			ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		  ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Refunds				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'R' ,
		'',
		ABS(SUM(Cash.TransC_curValue)), 
		tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue < 0
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
	  ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
	
/* Net			*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'N' ,
		'',
		SUM(Cash.TransC_curValue), 
		tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate 
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
	 ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
		
/* Gross Totals				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'G' ,
		'',
		SUM(Cash.TransC_curValue), 
		'TOTALS',
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue >= 0 
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Refunds Totals				*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'R' ,
		'',
		ABS(SUM(Cash.TransC_curValue)), 
		'TOTALS',
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		isNull(tblPaymentType.PayType_strGroupInTenderCat, 'N') ,
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue < 0
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
	
/* Net Totals			*/
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'N' ,
		'',
		SUM(Cash.TransC_curValue), 
		'TOTALS',
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		isNull(tblPaymentType.PayType_strGroupInTenderCat, 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate 
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
		
		
/* Gross Cinema Operator Totals				*/
IF @GroupByCinOperator = 'Y'
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'G' ,
		'',
		SUM(Cash.TransC_curValue), 
		'CIN OP TOTALS',
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue >= 0 
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	--tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
/* Refunds Cinema Operator Totals				*/
IF @GroupByCinOperator = 'Y'
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'R' ,
		'',
		ABS(SUM(Cash.TransC_curValue)), 
		'CIN OP TOTALS',
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		isNull(tblPaymentType.PayType_strGroupInTenderCat, 'N') ,
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate AND
		Cash.TransC_curValue < 0
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	--tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
	
/* Net Cinema Operator Totals			*/
IF @GroupByCinOperator = 'Y'
INSERT INTO tblRptReceiptSum
( 	
		RS_strType,
		Cinema_strCode,
		RS_curTranValue,
		RS_strWGroupDesc, 
		RS_strTenderCategory,  
		RS_strPayTypeDesc,
		RS_strGroupInTenderCat,
	  strCinOperatorCode
 )
SELECT 	'N' ,
		'',
		SUM(Cash.TransC_curValue), 
		'CIN OP TOTALS',
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		isNull(tblPaymentType.PayType_strGroupInTenderCat, 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode ) AS strCinOperatorCode
FROM tblTrans_Cash Cash WITH (INDEX=indDateTime), 
		tblPaymentType, tblWorkstation, tblWorkstation_Group, #tblTempRptReceiptSum
WHERE Cash.TransC_strType = tblPaymentType.PayType_strType AND
		Cash.Workstation_strCode = tblWorkstation.Workstation_strCode AND
		tblWorkstation.WGroup_strCode = tblWorkstation_Group.WGroup_strCode AND
		Cash.TransC_dtmDateTime BETWEEN @fromdate AND @todate 
		AND #tblTempRptReceiptSum.TransC_lgnNumber = Cash.TransC_lgnNumber
		AND #tblTempRptReceiptSum.TransC_intSequence = Cash.TransC_intSequence
    -- 15 May 2013 MVW  B31656  For credit/debit card transactions get payment type from tblCardDefinition:
    AND 'Y' IN ( tblPaymentType.PayType_strCreditCard, tblPaymentType.PayType_strDebitCard )
GROUP BY 	--tblWorkstation_Group.WGroup_strDescription,
		(CASE WHEN ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N') = 'N' THEN
		#tblTempRptReceiptSum.TransC_strBKCardType
		ELSE
		tblPaymentType.PayType_strTenderCategory END),
		#tblTempRptReceiptSum.TransC_strBKCardType,
		ISNULL(tblPaymentType.PayType_strGroupInTenderCat , 'N'),
		ISNULL( tblWorkstation_Group.WGroup_strDefaultCinOperator, @strDefaultCinOperatorCode )
		--delete from tblRptReceiptSum where upper(RS_strPayTypeDesc) like '%SK CARD%'
		--delete from tblRptReceiptSum where upper(RS_strPayTypeDesc) in ('LANDBANK 2','LANDBANK 3','LANDBANK 4','LANDBANK 5','LANDBANK 6','LANDBANK 7','LANDBANK 8','LANDBANK 9','LANDBANK 10','LANDBANK 1')
UPDATE tblRptReceiptSum
SET    Cinema_strCode           = @CinemaCode,
       CinOperator_intReportSeq = ( SELECT TOP 1 CinOperator_intReportSeq FROM tblCinema_Operator 
		                                WHERE CinOperator_strCode = strCinOperatorCode ),
	     CinOperator_strName      = ( SELECT TOP 1 CinOperator_strName FROM tblCinema_Operator 
		                                WHERE CinOperator_strCode = strCinOperatorCode ),
	     GroupByCinOperator_Param = @GroupByCinOperator

